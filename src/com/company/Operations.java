package com.company;

public class Operations {
	public double sumOfTwoNumbers(double number1, double number2) {
		return number1 + number2;
	}

	public double mathOperations(double number1, double number2, String operation) {
		switch(operation) {
			case "sum":
				return number1 + number2;
			case "subtract":
				return number1 - number2;
			case "multiply":
				return number1 * number2;
			case "divide":
				return number1 / number2;
		};

		return 0;
	}

	public int convertCentimetersToMeters(int centimeters) {
		return (int)((double)centimeters / 100);
	}

	public void passedSeconds(int seconds) {
		if(seconds < 0) {
			return;
		}

		int secondsPerDay = 86400;
		int secondsPerHour = 3600;
		int secondsPerMinute = 60;

		while(seconds > secondsPerDay) {
			seconds -= secondsPerDay;
		}

		System.out.println("Hours have passed since beginning of day: " + (int)((double)seconds / secondsPerHour));

		int passedHoursInSeconds = secondsPerHour * (int)((double)seconds / secondsPerHour);
		int remainingSeconds = seconds - passedHoursInSeconds;
		int passedMinutes = remainingSeconds / secondsPerMinute;
		int passedMinutesInSeconds = passedMinutes * secondsPerMinute;

		System.out.println("Minutes have passed since beginning of hour: " + passedMinutes);
		System.out.println("Seconds have passed since beginning of minute: " + (remainingSeconds - passedMinutesInSeconds));
	}

	public double compareThreeNumbers(double number1, double number2, double number3) {
		if(number1 > number2 && number1 > number3) {
			return number1;
		} else if(number2 > number1 && number2 > number3) {
			return number2;
		}

		return number3;
	}

	public String getNameOfDay(int numberOfDay) {
		switch(numberOfDay) {
			case 1:
				return "Monday";
			case 2:
				return "Tuesday";
			case 3:
				return "Wednesday";
			case 4:
				return "Thursday";
			case 5:
				return "Friday";
			case 6:
				return "Sunday";
			case 7:
				return "Saturday";
		};

		return "Incorrect number of day";
	}

	public boolean checkEnding(String string1, String string2) {
		if(string2.length() > string1.length()) {
			return false;
		}

		int difference = string1.length() - string2.length();

		for(int i = 0; i < string2.length(); ++i) {
			if(string1.charAt(i + difference) != string2.charAt(i)) {
				return false;
			}
		}

		return true;
	}

	public double findAverage(double[] array) {
		if(array.length < 1) {
			return 0;
		}

		double average = 0;
		for(int i = 0; i < array.length; ++i) {
			average += array[i];
		}

		return average / array.length;
	}

	public String move(String input) {
		char[] buffer = input.toCharArray();

		for(int i = 0; i < input.length(); ++i) {
			if((buffer[i] >= 'A' && buffer[i] < 'Z') || (buffer[i] >= 'a' && buffer[i] < 'z')) {
				buffer[i]++;
			}
			if(buffer[i] == 'Z') {
				buffer[i] = 'A';
			}
			if(buffer[i] == 'z') {
				buffer[i] = 'a';
			}
		}

		return buffer.toString();
	}
}
