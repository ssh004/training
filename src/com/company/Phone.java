package com.company;

public class Phone {
	protected String number;
	protected String model;
	protected double weight;

	public Phone(String number, String model, double weight) {
		this.number = number;
		this.model = model;
		this.weight = weight;
	}

	public void receiveCall(String name) {
		System.out.println("Звонит " + name);
	}

	public void receiveCall(String nameSender, String phoneNumber) {
		System.out.println("Звонит " + nameSender + " с номера " + phoneNumber);
	}

	public String getNumber() {
		return this.number;
	}

	public String getModel() {
		return this.model;
	}

	public double getWeight() {
		return this.weight;
	}
}
