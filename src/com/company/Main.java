package com.company;

public class Main {
	public static void main(String[] args) {
		Phone samsung = new Phone("79600134550", "Galaxy S21 Ultra 5G", 0.229);
		Phone motorola = new Phone("79135514276", "One Fusion+", 0.21);
		Phone phillips = new Phone("74180567080", "S561", 0.152);

		System.out.println("Model: " + samsung.getNumber()
				+ ", Model: " + samsung.getModel() + ", Weight: " + samsung.getWeight());

		System.out.println("Model: " + motorola.getNumber()
				+ ", Model: " + motorola.getModel() + ", Weight: " + motorola.getWeight());

		System.out.println("Model: " + phillips.getNumber()
				+ ", Model: " + phillips.getModel() + ", Weight: " + phillips.getWeight());

		samsung.receiveCall("Hooin Kyoma");
		motorola.receiveCall("Khovansky");
		phillips.receiveCall("Haruka", "814456615061");
	}
}
