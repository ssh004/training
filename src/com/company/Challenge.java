package com.company;

public class Challenge {
	public static String reverseCase(String str) {
		char[] buffer = str.toCharArray();

		for(int i = 0; i < str.length(); ++i) {
			if(buffer[i] > 64 && buffer[i] < 91) {
				buffer[i] += 32;
			} else if(buffer[i] > 96 && buffer[i] < 123) {
				buffer[i] -= 32;
			}
		}

		return new String(buffer);
	}
}
